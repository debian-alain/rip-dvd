#!/bin/bash

PATH=$PATH:/sbin:/usr/sbin

#### Golgot200 ###############################################################################
# 22-03-2019
# Archlinux
# MPlayer SVN-r38101
# ffmpeg version n4.1.1
# lsdvd 0.17
# ogmtools v1.5 (dvdxchap)
# mkvtoolnix-cli 32.0.0-1 (mkvmerge)
# mencoder-38125-5
##############################################################################################
# Modifications par enicar 09-18/06/2019
##############################################################################################

declare -a MPLAYER_IDENTIFY
declare -a LSDVD_X

mplayer_identify () {
    if [[  "${#MPLAYER_IDENTIFY[@]}" -eq 0 ]]; then
        mapfile MPLAYER_IDENTIFY < \
                <(mplayer -noconfig all -nocache -vo null -ao null -frames 0 \
                          -identify -dvd-device "$DEVICE" dvd://"$1" 2>/dev/null)
    fi
    echo -n "${MPLAYER_IDENTIFY[@]}"
}

awk_lang () {
    local ilang="$1"
    local olang="$2"
    awk "/language: $ilang/ && /format:/ \
         {printf \"-metadata:s:a:\"\$3\" language=$olang -metadata:s:a:\"\$3\" title=\"\$5\$6\" \"}"
}

awk_lpcm () {
    local ilang="$1"
    local olang="$2"
    awk "/language: $ilang/ && /format: lpcm/ \
         {printf \"-metadata:s:a:0 language=$olang -metadata:s:a:0 title=\"\$5\$6\" \"}"
}

lsdvd_x () {
    if [[ "${#LSDVD_X[@]}" -eq 0  ]]; then
        mapfile LSDVD_X < <(lsdvd -x "$DEVICE" -t "$1" 2> /dev/null)
    fi
    echo -n "${LSDVD_X[@]}"
}

merge_chapters () {
    local output="$2"
    dvdxchap -t  "$1" "$DEVICE" > "$FOLDER/$1-$TITLE-CHAPTERS.txt"
    mkvmerge "$FOLDER/$1-$TITLE.mkv" --chapters "$FOLDER/$1-$TITLE-CHAPTERS.txt" \
             -o "$output"

    rm -f "$FOLDER/$1-$TITLE.mkv"
    rm -f "$FOLDER/$1-$TITLE-CHAPTERS.txt"
}

encode_sidfr_and_merge () {
    local output="$2"
    for n in $SIDFR; do
        mencoder -dvd-device "$DEVICE" "dvd://$1" \
                 -nosound -ovc copy -force-avi-aspect "$RATIO" \
                 -o /dev/null -ifo "$MOUNT_POINT/VIDEO_TS/VTS_01_0.IFO" \
                 -sid "$n" -vobsubout "$FOLDER/$i-vobsubs-fr" -vobsuboutindex "$n"
    done

    mkvmerge "$FOLDER/$1-$TITLE+CHAPTERS.mkv" -a fre --default-language fr \
             "$FOLDER"/*.idx -o "$output"
}

menage () {
    rm -f "$FOLDER"/*.idx
    rm -f "$FOLDER"/*.sub
    rm -f "$FOLDER/$1-$TITLE+CHAPTERS.mkv"
}

## CONDITION : UN SEUL DVD-VIDÉO
mapfile -t  BLKID < <(blkid /dev/sr* |sed -E 's/^([^:]+): .*/\1/')

if [[ "${#BLKID[@]}" -eq  "0" ]] ; then
    echo "Aucun DVD trouvé: abandon"
    exit 0
fi

idx=0
for dev in "${BLKID[@]}"; do
    #mnt=$(awk "/${dev//\//\\\/}/ { print \$2 }" </proc/mounts)
    mnt=$(mount |sed -n -E "s/^${dev//\//\\\/} on (.*) type .*\$/\\1/p")
    [[ -z "$mnt" ]] && continue
    if [[ -f "$mnt/VIDEO_TS/VIDEO_TS.IFO" ]]; then
        DEVICES[$idx]="$dev"
        MOUNT_POINTS[$idx]="$mnt"
        idx=$((idx+1))
    fi
done

if [[ "${#DEVICES[@]}" -eq 0 ]]; then
    echo "Aucun dvd vidéo monté : Abandon"
    exit 0
elif [[ "${#DEVICES[@]}" -gt 1 ]];  then
    echo "Il y a plus d'un dvd video détecté : Abandon"
    exit 0
fi

DEVICE=${DEVICES[0]}
MOUNT_POINT=${MOUNT_POINTS[0]}

## LABEL DU DVD-VIDÉO & NOMBRE TOTAL DE TITRES SUR CELUI-CI
idx=0
TITLE_COUNT=0
regexp='^Title: '
while read -r line; do
    idx=$((idx+1))
    if [[ "$idx" -eq 1 ]]; then 
        TITLE=${line#*: }
    elif [[ "$line" =~ $regexp ]]; then
        TITLE_COUNT=$((TITLE_COUNT+1))
    fi
done < <(lsdvd "$DEVICE" 2>/dev/null)

unset regexp dev mnt idx BLKID DEVICES MOUNT_POINTS

if [[ -z "$TITLE" ]]; then
    echo "Pas de dvd vidéo trouvé : abandon"
    exit 0
fi

echo "Proceed..."

DIR=$(xdg-user-dir DESKTOP)
FOLDER="$DIR/$TITLE"

if [[ ! -d "$FOLDER" ]]; then
    echo "$FOLDER n'existe pas !"
    echo "Création Du Dossier $FOLDER"
    mkdir -p "$FOLDER"
fi

for (( i=1; i<=TITLE_COUNT; i++ ))
do
    MPLAYER_IDENTIFY=()
    LSDVD_X=()
    ## TITRES D'UNE DURÉE DE PLUS DE .... SECONDES [ ICI, 1200 SECONDES = 20 MINUTES]
    #LENGTH=$(mplayer_identify "$i" | grep ID_LENGTH | sed -E 's/.*=([0-9]*)/\1/' | sed 's/\..*//')
    LENGTH=$(mplayer_identify "$i" |sed -E -n 's/^.*ID_LENGTH=([0-9]+).*$/\1/p')
    LIMIT=1200
    [[ "$LENGTH" -le "$LIMIT" ]] && continue

## METADATAS DIVERS [À COMPLETER AU CAS OÙ IL MANQUERAIT QUELQUES LANGUES]
## https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1
    FR=$(mplayer_identify "$i" | awk_lang fr fra)
    FR_LPCM=$(mplayer_identify "$i" | awk_lpcm fr fra)
    EN=$(mplayer_identify "$i" | awk_lang en en)
    EN_LPCM=$(mplayer_identify "$i" | awk_lpcm en en)
    DE=$(mplayer_identify "$i" | awk_lang de ger)
    DE_LPCM=$(mplayer_identify "$i" | awk_lpcm de ger)
    IT=$(mplayer_identify "$i" | awk_lang it ita)
    IT_LPCM=$(mplayer_identify "$i" | awk_lpcm it ita)
    ES=$(mplayer_identify "$i" |awk_lang es spa)
    ES_LPCM=$(mplayer_identify "$i" |awk_lpcm es spa)
    NL=$(mplayer_identify "$i" |awk_lang nl nld)
    NL_LPCM=$(mplayer_identify "$i" |awk_lpcm nl nld)
    HE=$(mplayer_identify "$i" |awk_lang he heb)
    HE_LPCM=$(mplayer_identify "$i" |awk_lpcm he heb)
    HU=$(mplayer_identify "$i" |awk_lang hu hun)
    HU_LPCM=$(mplayer_identify "$i" |awk_lpcm hu hun)
    PL=$(mplayer_identify "$i" |awk_lang pl pol)
    PL_LPCM=$(mplayer_identify "$i" |awk_lpcm pl pol)
    CS=$(mplayer_identify "$i" |awk_lang cs cze)
    CS_LPCM=$(mplayer_identify "$i" |awk_lpcm cs cze)
    JA=$(mplayer_identify "$i" |awk_lang ja jpn)
    JA_LPCM=$(mplayer_identify "$i" |awk_lpcm ja jpn)
    SV=$(mplayer_identify "$i" |awk_lang sv swe)
    SV_LPCM=$(mplayer_identify "$i" |awk_lpcm sv swe)
    UNKNOWN=$(mplayer_identify "$i" |awk_lang unknown und)
    UNKNOWN_LPCM=$(mplayer_identify "$i" |awk_lpcm unknown und)
    EMPTY_LANG=$(mplayer_identify "$i" |awk_lang ' aid:' und)
    EMPTY_LANG_LPCM=$(mplayer_identify "$i" |awk_lpcm ' aid:' und)

    TAGS_ID_STREAMS="${FR}${EN}${DE}${IT}${ES}${NL}${HE}${HU}${PL}${CS}${JA}${SV}${UNKNOWN}${EMPTY_LANG}"
    TAGS_ID_STREAMS_LPCM="${FR_LPCM}${EN_LPCM}${DE_LPCM}${IT_LPCM}${ES_LPCM}${NL_LPCM}${HE_LPCM}${HU_LPCM}${PL_LPCM}${CS_LPCM}${JA_LPCM}${SV_LPCM}${UNKNOWN_LPCM}${EMPTY_LANG_LPCM}"

    COUNT_LPCM=$(lsdvd_x "$i" |grep -c 'lpcm')
    COUNT_AC3_DTS_MPEG1=$(lsdvd_x "$i" | grep -c -E 'ac3,|dts,|mpeg1,')

    MAP_ID_STREAMS=$(lsdvd_x "$i" |sed -E -n '/Audio:/s/.*(0x)/\1/p' | \
                         awk '{ gsub(/c/, "1c") }
                              /0x/ {gsub (/^/,"-map i:")}
                              { print $0 }')

    MAP_ID_STREAMS_LPCM=$(lsdvd_x "$i" | sed -n -E '/lpcm/s/.*(0x)/\1/p' | \
                              awk '{ gsub(/c/, "1c") }
                                   /0x/ { gsub (/^/,"-map i:")}
                                   { print $0 }')

    SIDFR=$(lsdvd_x "$i" |awk '/Subtitle:/ && /Language: fr/ { sub(/,$/ ,"", $2);
                                     if ($2 ~ /^[0-9]+$/) print (--$2) }')

    # COUNT_SID_FR=$(mplayer_identify "$i" |grep "subtitle ( sid ):" | \
    #                    grep -c "language: fr")
    COUNT_SID_FR=$(echo "$SIDFR" |wc -w)

    RATIO=$(lsdvd_x "$i" | \
                awk 'NR <= 3 && /Aspect ratio:/ {sub(/\//, ":", $11); x = x " " $11}
                          END { sub(/.$/, "", x); print x}')

    INFO_AC3_DTS_MPEG1=$(lsdvd_x "$i" | \
                awk '/ac3,|dts,|mpeg1,/  { x = x $4 ","}
                    END { sub(/,*$/, "", x); print x }')

    INFO_LPCM=$(lsdvd_x "$i" | awk '/lpcm/ { print $4 }')


    mplayer -noconfig all -nocache -dvd-device "$DEVICE" dvd://"$i" \
            -dumpstream -dumpfile "$FOLDER/$i-$TITLE.vob"

    ## ENCODAGE
    ## FILMS
    ## AC3 & DTS & MPEG1
    ## SOUS-TITRAGE(S) VF
    if [[ "$COUNT_LPCM" -eq "0" ]] && \
        [[ "$COUNT_AC3_DTS_MPEG1" -ge "1" ]] && \
        [[ "$COUNT_SID_FR" -ge "1" ]]
    then
        ffmpeg -y -i "$FOLDER/$i-$TITLE.vob" \
            -map 0:v -metadata title="$TITLE" \
            -c:v libx264 -preset slow -profile:v high -level 4.2 \
            -loglevel repeat+verbose  ${MAP_ID_STREAMS} \
            -c:a copy $TAGS_ID_STREAMS -movflags +faststart -qmin 18 -qmax 23 \
            "$FOLDER/$i-$TITLE.mkv"

        rm -f "$FOLDER/$i-$TITLE.vob"
        merge_chapters "$i" "$FOLDER/$i-$TITLE+CHAPTERS.mkv"
        encode_sidfr_and_merge "$i" "$FOLDER/$i-$TITLE-[Language:$INFO_AC3_DTS_MPEG1]+SUBVF.mkv"
        menage "$i"

    ## ENCODAGE
    ## CONCERTS LIVE
    ## LPCM (ON GARDE QUE LA PISTE LPCM)
    ## PAS DE SOUS-TITRAGE

    elif [[ "$COUNT_LPCM" -ge "1" ]] && \
        [[ "$COUNT_AC3_DTS_MPEG1" -ge "0" ]] && \
        [[ "$COUNT_SID_FR" -eq "0" ]]
    then
        ffmpeg -i "$FOLDER/$i-$TITLE.vob" \
               -map 0:v -metadata title="$TITLE" \
               -c:v libx264 -preset hq -profile:v high -level 4.2 \
               -loglevel repeat+verbose  ${MAP_ID_STREAMS_LPCM} \
               -acodec pcm_s16be $TAGS_ID_STREAMS_LPCM \
               -movflags +faststart -qmin 18 -qmax 23 -y "$FOLDER/$i-$TITLE.mkv"

        rm -f "$FOLDER/$i-$TITLE.vob"
        merge_chapters "$i" "$FOLDER/$i-$TITLE-[Language:$INFO_LPCM].mkv"

    ## ENCODAGE
    ## CONCERTS LIVE
    ## LPCM (ON GARDE QUE LA PISTE LPCM)
    ## SOUS-TITRAGE(S) VF

    elif [[ "$COUNT_LPCM" -ge "1" ]] && \
            [[ "$COUNT_AC3_DTS_MPEG1" -ge "0" ]] && \
            [[ "$COUNT_SID_FR" -ge "1" ]]
    then
        ffmpeg  -i "$FOLDER/$i-$TITLE.vob" \
            -map 0:v -metadata title="$TITLE" \
            -c:v libx264 -preset hq -profile:v high -level 4.2 \
            -loglevel repeat+verbose  ${MAP_ID_STREAMS_LPCM} \
            -acodec pcm_s16be $TAGS_ID_STREAMS_LPCM -movflags +faststart \
            -qmin 18 -qmax 23 -y "$FOLDER/$i-$TITLE.mkv"

        rm -f "$FOLDER/$i-$TITLE.vob"
        merge_chapters "$i" "$FOLDER/$i-$TITLE+CHAPTERS.mkv"
        encode_sidfr_and_merge "$i" "$FOLDER/$i-$TITLE-[Language:$INFO_LPCM]+SUBVF.mkv"
        menage "$i"

    elif [[ "$COUNT_LPCM" -eq "0" ]] && \
            [[ "$COUNT_AC3_DTS_MPEG1" -ge "1" ]] && \
            [[ "$COUNT_SID_FR" -eq "0" ]]
    then
        ffmpeg -i "$FOLDER/$i-$TITLE.vob" \
            -map 0:v -metadata title="$TITLE" \
            -c:v libx264 -preset hq -profile:v high -level 4.2 \
            -loglevel repeat+verbose ${MAP_ID_STREAMS} \
            -c:a copy ${TAGS_ID_STREAMS} -movflags +faststart \
            -qmin 18 -qmax 23 -y "$FOLDER/$i-$TITLE.mkv"

        rm -f "$FOLDER/$i-$TITLE.vob"
        merge_chapters "$i" "$FOLDER/$i-$TITLE-[Language:$INFO_AC3_DTS_MPEG1].mkv"
    fi
done
